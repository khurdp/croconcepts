const express = require('express');
const path = require('path');
const app = express();
app.use(express.static(__dirname + '/dist/croconcepts'));  // myapp is the "name" of application in package.json.
app.get('/*', function(req,res) {
res.sendFile(path.join(__dirname+
		'/dist/croconcepts/index.html'));});
app.listen(process.env.PORT || 8080);
